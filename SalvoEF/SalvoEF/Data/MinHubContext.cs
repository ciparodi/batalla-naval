﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalvoEF.Models;

namespace SalvoEF.Data
{
    public class MinHubContext : DbContext
    {
        public MinHubContext()
        {
        }

        public MinHubContext (DbContextOptions<MinHubContext> options)
            : base(options)
        {
        }

        public DbSet<Player> Player { get; set; }
        public DbSet<Game> Game { get; set; }
        public DbSet<GamePlayer> GamePlayer { get; set; }
        public DbSet<Ship> Ship { get; set; }
        public DbSet<ShipLocation> ShipLocation { get; set; } 
        public DbSet<Salvo> Salvo { get; set; }
        public DbSet<Score> Scores { get; set; }
    }
}
