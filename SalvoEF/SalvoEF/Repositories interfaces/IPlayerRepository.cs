﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Repositorys
{
    public interface IPlayerRepository
    {
        Player FindByEmail(string email);
        void Save(Player player);
    }
}
