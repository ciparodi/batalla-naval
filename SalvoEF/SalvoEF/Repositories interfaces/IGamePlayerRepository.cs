﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Repositorys
{
    public interface IGamePlayerRepository
    {
        List<GamePlayer> GetGamePlayerList();
        GamePlayer GetGameGPID(long idGame);
        GamePlayer GetGamePlayerID(long idGamePlayer);
        void Save(GamePlayer gamePlayer);
        GamePlayer FindByID(long Id);
    }
}
