﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Repositorys
{
    public interface IGameRepository
    {
        Game GetGameForView(long id);
        List<Game> GetGames();
        void Save(Game game);
        Game FindByID(long ID);

    }
}
