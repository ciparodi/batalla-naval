﻿using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Repositories_interfaces
{
    public interface IScoreRepository
    {
        void Save(Score score);
    }
}
