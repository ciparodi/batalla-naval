﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalvoEF.Models;
using SalvoEF.Repositorys;
using SalvoEF.Data;
using salvoEF;
using SalvoEF.ModelDTO;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace SalvoEF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class GamesController : ControllerBase
    {

        private IGameRepository gameRepository;
        private IPlayerRepository playerRepository;
        private IGamePlayerRepository gamePlayerRepository;


        public GamesController(IGameRepository repositoryg, IPlayerRepository repositoryp, IGamePlayerRepository repositorygp)
        {
            gameRepository = repositoryg;
            playerRepository = repositoryp;
            gamePlayerRepository = repositorygp;

        }


        // GET: api/Games
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetGames()
        {
            string userEmail;

            userEmail = User.FindFirst("Player") != null
                ? User.FindFirst("Player").Value
                : "Guest";

            List<Game> gamesBD = gameRepository.GetGames();

            List<GameDTO> gameListDTO = gamesBD.Select(x => new GameDTO(x)).ToList();

            var gameAuth = new GameAuthDTO();
            gameAuth.Email = userEmail;
            gameAuth.Games = gameListDTO;


            return Ok(gameAuth);
        }

        [HttpGet("{id}")]
        public IActionResult GetGameUnique(long id)
        {

            var gameIDBD = gameRepository.GetGameForView(id);
            var gameIDDTO = new GameDTO(gameIDBD);

            return Ok(gameIDDTO);
        }

        [HttpPost]
        
        public IActionResult Post()
        {
            try
            {

                Player user = playerRepository.FindByEmail(User.FindFirst("Player").Value);
                if (user == null)
                    return StatusCode(403, "El usuario no existe. Ingrese un usuario para continuar.");

                Game game = new Game();
                game.CreationDate = DateTime.Now;

                GamePlayer nuevoGamePlayer = new GamePlayer();

                nuevoGamePlayer.JoinDate = DateTime.Now;            
                nuevoGamePlayer.Playerid = user.Id;           
                nuevoGamePlayer.Game = game;

                gamePlayerRepository.Save(nuevoGamePlayer);
                

                return StatusCode(201, nuevoGamePlayer.Id);
            }

            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }


        }
            

        [HttpPost("{Id}/players")]

        public IActionResult Post([FromRoute] long Id)
        {
            try
            {

                Game game = gameRepository.FindByID(Id);

                if (game == null)
                    return StatusCode(403, "El juego no existe.");

                Player user = playerRepository.FindByEmail(User.FindFirst("Player").Value);

                foreach (GamePlayer gp in game.GamePlayer)
                {
                    if (user == gp.Player)
                        return StatusCode(403, "El jugador ya está en el juego");
                }

                if (game.GamePlayer.Count() > 1)
                    return StatusCode(403, "El juego está lleno.");



                GamePlayer newGamePlayer = new GamePlayer();

                newGamePlayer.Gameid = game.Id;
                newGamePlayer.Playerid = user.Id;
                newGamePlayer.JoinDate = DateTime.Now;


                gamePlayerRepository.Save(newGamePlayer);

                return StatusCode(201, newGamePlayer.Id);
            }

            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
           
        }

    }
}