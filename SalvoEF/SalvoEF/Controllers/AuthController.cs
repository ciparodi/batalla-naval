﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using salvoEF;
using SalvoEF.Models;
using SalvoEF.Repositorys;

namespace SalvoEF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IPlayerRepository playerRepository;

        public AuthController(IPlayerRepository repository)
        {
            playerRepository = repository;
        }


        // POST: api/Auth/login
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] PlayerDTO player)
        {
            try
            {
                Player user = playerRepository.FindByEmail(player.Email);

                //si user es null o user.password no es igual a player.password
                //devuelve unauthorized
                if (user == null || !String.Equals(user.Password, player.Password))
                    return Unauthorized();


                //asigna a una variable los claim
                var claims = new List<Claim>
                    {
                        new Claim("Player", user.Email),                        
                    };

                
                //asigna a una variable el objeto ClaimsIdentity que toma los claims anteriores
                //y valida con el cookieAuthentication
                var claimsIdentity = new ClaimsIdentity(
                    claims, CookieAuthenticationDefaults.AuthenticationScheme);

                //espera y en httpContext usa la funcion SignInAsync tomando como variable
                //claimsprincipal y valida con el cookieAuthentication           
                await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(claimsIdentity));

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

        // POST: api/Auth/logout
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
