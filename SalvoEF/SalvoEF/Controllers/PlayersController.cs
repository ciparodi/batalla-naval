﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalvoEF.Models;
using SalvoEF.Data;
using SalvoEF.Repositorys;
using salvoEF;

namespace SalvoEF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private IPlayerRepository playerRepository;

        public PlayersController(IPlayerRepository repository)
        {
            playerRepository = repository;
        }



        [HttpGet("{email}")]

        public IActionResult FindByMail( string email)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var player = playerRepository.FindByEmail(email);

            if (player == null)
            {
                return NotFound();
            }

            return Ok(player);
        }

        [HttpPost]

        public IActionResult Post([FromBody]PlayerDTO player)
        {

            try
            {

                if (String.IsNullOrWhiteSpace(player.Email) || String.IsNullOrWhiteSpace(player.Password) )
                    return StatusCode(403, "Datos inválidos");

                Player newplayer = playerRepository.FindByEmail(player.Email);

                if (newplayer != null)
                    return StatusCode(403, "Email en uso");

                newplayer = new Player();
                newplayer.Email = player.Email;
                newplayer.Password = player.Password;

                playerRepository.Save(newplayer);


                return StatusCode(201, "Usuario creado");
            }

            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

    }
}