﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalvoEF.Models;

using SalvoEF.Data;
using SalvoEF.Repositorys;
using salvoEF;
using SalvoEF.ModelDTO;
using Microsoft.AspNetCore.Authorization;
using SalvoEF.Repositories_interfaces;

namespace SalvoEF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize("PlayerOnly")]
    public class GamePlayersController : ControllerBase
    {
        public IGamePlayerRepository gamePlayerRepository;
        public IPlayerRepository playerRepository;
        public IGameRepository gameRepository;
        public IScoreRepository scoreRepository;


        public GamePlayersController(IGamePlayerRepository repository,
                                        IPlayerRepository repositoryp,
                                            IGameRepository repositoryg,
                                                IScoreRepository repositorysc)
        {
            gamePlayerRepository = repository;
            playerRepository = repositoryp;
            gameRepository = repositoryg;
            scoreRepository = repositorysc;
        }

        [HttpGet]
        public IActionResult GetGamePlayerList()
        {

            var gamePBD = (gamePlayerRepository.GetGamePlayerList());
            List<GamePlayerDTO> gameDTO = gamePBD.Select(x => new GamePlayerDTO(x)).ToList();

            //return salvo.Select(x => new SalvoDTO(x)).ToList();

            //foreach (GamePlayer gp in gamePBD)
            //{
            //    var gpdto = new GamePlayerDTO(gp);
            //    gameDTO.Add(gpdto);
            //}

            
            return Ok(gameDTO);
        }

        [HttpGet("{id}")]
        public IActionResult GetGameGPID(long id)
        {

            var gamedb = gamePlayerRepository.GetGamePlayerID(id);
            var gameIDDTO = new GameViewDTO(gamedb);
            string userEmail = User.FindFirst("Player").Value;

            if (gamedb.Player.Email != userEmail)
            { return StatusCode(404, "Forbiden"); }

            return Ok(gameIDDTO);
        }

        [HttpPost("{id}/ships")]
        public IActionResult SetShips(long Id, [FromBody] List<ShipDTO> ships)
        {
            try
            {

                GamePlayer gamePlayer = gamePlayerRepository.FindByID(Id);

                Player player = playerRepository.FindByEmail(User.FindFirst("Player").Value);

                List<Ship> barcos = ships.Select(x => ShipDTO.ShipDTOtoShip(x)).ToList();

                //foreach (ShipDTO sdto in ships)
                //{
                //    barcos.Add(ShipDTO.ShipDTOtoShip(sdto));
                //}


                if (gamePlayer == null)
                    return StatusCode(403, "No existe");

                if (gamePlayer.Playerid != player.Id)
                    return StatusCode(403, "Este no es tu juego!");

                if (gamePlayer.Ships.Count() != 0)
                    return StatusCode(403, "Ya posicionaste tus barcos");


                gamePlayer.Ships = barcos;

                gamePlayerRepository.Save(gamePlayer);

                return StatusCode(201, gamePlayer.Id);
            }

            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost("{id}/salvos")]
        public IActionResult SetSalvos(long Id, [FromBody] SalvoDTO salvodto)
        {

            GamePlayer gamePlayer = gamePlayerRepository.FindByID(Id);

            GamePlayer gpopponent = gamePlayer.GetOpponent();

            Player player = playerRepository.FindByEmail(User.FindFirst("Player").Value);

            Salvo salvo = SalvoDTO.SalvoDTOtoSalvo(salvodto);


            //valida si existe el jugador
            if (gamePlayer == null)
                return StatusCode(403, "No existe");

            // valida si es el juego de ese jugador
            if (gamePlayer.Playerid != player.Id)
                return StatusCode(403, "Este no es tu juego!");

            // valida si tiene oponente
            if (gpopponent == null)
                return StatusCode(403, "No hay oponente");

            // valida si el oponente puso sus naves
            if (gpopponent.Ships.Count() == 0 || gpopponent.Ships == null)
                return StatusCode(403, "Tu oponente aún no posicionó sus barcos.");

            //valida si empezo a jugar el oponente (es el primero que juega)
            if (gpopponent.JoinDate < gamePlayer.JoinDate && gpopponent.Salvos == null)
                return StatusCode(403, "Esperá a que juegue tu oponente");

            //valida si es su turno                
            if ((gpopponent.Salvos.Count() == gamePlayer.Salvos.Count()
                && gpopponent.JoinDate < gamePlayer.JoinDate)
                || (gpopponent.Salvos.Count() < gamePlayer.Salvos.Count()))
                return StatusCode(403, "No es tu turno!");

            //valida si la lista esta vacía y asigna número de turno.
            if (gamePlayer.Salvos.Count() != 0)
                salvo.Turn = gamePlayer.Salvos.Count + 1;//Last().Turn + 1;
            else
                salvo.Turn = 1;

           
            gamePlayer.Salvos.Add(salvo);

            if (gamePlayer.GameOver())
            {
                
                scoreRepository.Save(Score.GetScore(gamePlayer.GetOpponent()));
                scoreRepository.Save(Score.GetScore(gamePlayer));
            }

            gamePlayerRepository.Save(gamePlayer);

            return StatusCode(201, gamePlayer.Id);
        }

        
    }
}
       
    