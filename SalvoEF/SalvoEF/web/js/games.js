$(function() {
    loadData()
});

function updateView(data) {
    let htmlList = data.map(function (games) {
          return  '<li>' + new Date(games.created).toLocaleString()
       + ' ' + games.gamePlayers.map(function(p) { return p.player.userName}).join(',')  +'</li>';
    }).join('');
  document.getElementById("game-list").innerHTML = htmlList;
}

function loadData() {
    $.get("http://localhost:56238/api/games")
        .done(function(data) {
          updateView(data);
        })
        .fail(function( jqXHR, textStatus ) {
          alert( "Failed: " + textStatus );
        });
}

/* function loadData() {
  $.get({
    url:"http://localhost:56238/api/games",
    type:"GET",
    contentType:"application/json; charset=utf-8"
  })
      .done(function(data) {
        updateView(data);
      })
      .fail(function( jqXHR, textStatus ) {
        alert( "Failed: " + textStatus );
      });
} */

