﻿using Microsoft.EntityFrameworkCore;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using SalvoEF.Data;
using SalvoEF;
using salvoEF;

namespace SalvoEF.Repositorys
{
    public class GamePlayerRepository : RepositoryBase<GamePlayer>, IGamePlayerRepository
    {


        public GamePlayerRepository(MinHubContext context)
        : base(context)
        {
        }

        //devuelve lista de GamePlayer e incluye los Player.
        public List<GamePlayer> GetGamePlayerList()
        {

 
            return FindAll(source => source.Include(gp => gp.Player))                                                                                              
                                          .ToList();

        }

        //devuelve GamePlayer filtrado según Game ID
        public GamePlayer GetGameGPID(long idGame)
        {

            return FindAll().FirstOrDefault(gpbd => gpbd.Gameid == idGame);

        }

        //devuelve GamePlayer según ID.
        public GamePlayer GetGamePlayerID(long idGamePlayer)
        {
                                            //busca ships en gameplayer, luego locations en ship.
            return FindAll(source => source.Include(gpbd => gpbd.Ships)
                                                .ThenInclude(ship => ship.Locations)
                                            //busca los salvos en gameplayer, luego locations en salvos.
                                            .Include(gpbd => gpbd.Salvos)
                                                .ThenInclude(salvo => salvo.Locations)
                                            //busca los game en gameplayer, luego gameplayer en ese game,
                                            //luego los player.
                                            .Include(gpbd => gpbd.Game)
                                                .ThenInclude(game => game.GamePlayer)
                                                    .ThenInclude(gp => gp.Player)
                                                        .ThenInclude(p => p.Scores)
                                            //busca los game en gameplayer, luego gameplayer en ese game,
                                            //luego salvos en gameplayer de game, luego locations de esos salvo.
                                            .Include(gpbd => gpbd.Game)
                                                .ThenInclude(game => game.GamePlayer)
                                                    .ThenInclude(gp => gp.Salvos)
                                                    .ThenInclude(salvo => salvo.Locations)
                                            //busca los game en gameplayer, luego los gameplayer en ese game,
                                            //luego los ships en gameplayer de game, luego locations de esos ships.
                                            .Include(gpbd => gpbd.Game)
                                                .ThenInclude(game => game.GamePlayer)
                                                    .ThenInclude(gp => gp.Ships)
                                                    .ThenInclude(ship => ship.Locations)                                            )
                                            .Where(gpbd => gpbd.Id == idGamePlayer)
                                            .OrderBy(game => game.JoinDate)
                                            .FirstOrDefault();

        }

        public void Save(GamePlayer gamePlayer)
        {
            if (gamePlayer.Id == 0)
                Create(gamePlayer);
            else
                Update(gamePlayer);
            
            SaveChanges();
        }

        public GamePlayer FindByID(long Id)
        {
            return FindAll(source => source.Include(gp => gp.Player)
                                            .Include(gp => gp.Game)
                                                .ThenInclude(g => g.GamePlayer) 
                                                    .ThenInclude(gp => gp.Salvos)
                                                         .ThenInclude(s => s.Locations)
                                            .Include(gp => gp.Game)
                                                .ThenInclude(g => g.GamePlayer)
                                                    .ThenInclude(gp => gp.Ships)
                                                        .ThenInclude(s => s.Locations))
                                             
                                            .FirstOrDefault(gp => gp.Id == Id);
        }

      
    }
}
