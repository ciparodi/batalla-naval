﻿
using SalvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using SalvoEF.Data;
using salvoEF;
using Microsoft.EntityFrameworkCore;

namespace SalvoEF.Repositorys
{


    public class PlayerRepository : RepositoryBase<Player>, IPlayerRepository
    {
        public PlayerRepository(MinHubContext context)
        : base(context)
        {
        }

        public Player FindByEmail(string email)
        {
            return FindAll().FirstOrDefault(p => p.Email == email);
        }

        public void Save (Player player)
        {
            Create(player);
            SaveChanges();
        }

    }
}
