﻿using SalvoEF.Data;
using SalvoEF.Models;
using SalvoEF.Repositories_interfaces;
using SalvoEF.Repositorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Repositories
{
    public class ScoreRepository : RepositoryBase<Score>, IScoreRepository
    {
        public ScoreRepository(MinHubContext context)
        : base(context)
        {
        }

        public void Save(Score score)
        {
            if (score.Id == 0)
                Create(score);
            else
                Update(score);
            
            SaveChanges();
        }
    }
}
