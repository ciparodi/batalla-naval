﻿using Microsoft.EntityFrameworkCore;
using salvoEF;
using SalvoEF;
using SalvoEF.Data;
using SalvoEF.Models;
using SalvoEF.Repositorys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Repositorys

{
    public class GameRepository : RepositoryBase<Game>, IGameRepository //Hereda de repositorio base, tomando la clase Game de la carpeta Models
    {
        public GameRepository(MinHubContext context)
        : base(context)
        {
        }


        //devuelve game por ID
        public Game GetGameForView(long id)
        {
            //trae GamePlayer, despues de ahí saca Player, y de ahí Scores.
            return FindAll(source => source.Include(game => game.GamePlayer)
                                               .ThenInclude(gameplayer => gameplayer.Player)
                                                    .ThenInclude(p => p.Scores)
                                           //trae GamePlayer, después de ahí saca Ships y de ahí Locations.
                                           .Include(x => x.GamePlayer)
                                               .ThenInclude(y => y.Ships)
                                                   .ThenInclude(b => b.Locations))
                                           .FirstOrDefault(x => x.Id == id);
        }


        public List<Game> GetGames()
        {
            //trae GamePlayer, de ahí Player, y de ahí Scores-
            return FindAll(source => source.Include(g => g.GamePlayer)
                                                  .ThenInclude(gp => gp.Player)
                                                        .ThenInclude(p => p.Scores)
                                            //trae GamePlayer, de ahí Ships, y de ahí Locations
                                            .Include(x => x.GamePlayer)
                                                  .ThenInclude(y => y.Ships)
                                                      .ThenInclude(b => b.Locations)
                                            //trae GamePlayer, de ahí Salvos y de ahí Locations.
                                            .Include(g => g.GamePlayer)
                                                    .ThenInclude(gp => gp.Salvos)
                                                        .ThenInclude(s => s.Locations))
                                                .ToList();
        }

        public Game FindByID(long ID)
        {
            return FindAll(source => source.Include(g => g.GamePlayer)).FirstOrDefault(g => g.Id == ID);
        }

        public void Save(Game game)
        {
            Create(game);
            SaveChanges();
        }
    }

}
