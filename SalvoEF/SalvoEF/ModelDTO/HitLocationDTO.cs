﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class HitLocationDTO
    {
        public string Type { get; set; }

        public List<string> Hits { get; set; }

        public HitLocationDTO(Ship ship, Salvo salvoOp)
        {
            Type = ship.Type;

            Hits = GetHitsLocationsString(salvoOp, ship); // bien

        }
        static public List<string> GetHitsLocationsString(Salvo salvoOp, Ship barcos)
        {
            List<string> listhit = new List<string>();

            {
                //compara las ubicaciones de mis barcos con las ubicaciones
                //de los disparos enemigos y agrega cada ubicacion a la lista
                foreach (ShipLocation locshi in barcos.Locations)
                {
                    foreach (SalvoLocations locsal in salvoOp.Locations)
                    {
                        if (locsal.Location == locshi.Location)
                        {
                            listhit.Add(locshi.Location);
                        }
                    }

                }
            }

            return listhit;
        }

        static public List<HitLocationDTO> ListHitLocationDTO(List<Ship> ship, Salvo salvoOp)
        {
            var listaHitLocDTO = new List<HitLocationDTO>();
            foreach (Ship s in ship)
            {
                var hitlocDTO = new HitLocationDTO(s, salvoOp);
                listaHitLocDTO.Add(hitlocDTO);
            }

            return listaHitLocDTO;
        }


        static public List<HitLocationDTO> ListHitLocationDTO(GamePlayer gamePlayer)
        {
            var listaHitLocDTO = new List<HitLocationDTO>();

            foreach (var gp in gamePlayer.Game.GamePlayer)
            {
                foreach (Ship ship in gp.Ships)
                {
                    foreach (Salvo salvo in gp.GetOpponent().Salvos)
                    {
                        var hitlocDTO = new HitLocationDTO(ship, salvo);
                        listaHitLocDTO.Add(hitlocDTO);
                    }
                }

            }
            return listaHitLocDTO;
        }
    }
}








            /*foreach (var gp in gamePlayer.Game.GamePlayer)
            {
                var Hit = new HitDTO(gamePlayer);
                foreach (var salvo in gamePlayer.Salvos)
                {
                    var salvoOp = new Salvo();
                    var shipMio = new Ship();

                    foreach (var sal in gamePlayer.GetOpponent().Salvos)
                    {
                        salvoOp = sal;
                    }

                    foreach (var shi in gamePlayer.Ships)
                    {
                        shipMio = shi;
                    }

                    Hit.Turn = salvo.Turn;
                    Hit.Hits = 

                }*/
 
