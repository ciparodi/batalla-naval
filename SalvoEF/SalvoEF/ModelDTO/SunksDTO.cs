﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class SunksDTO
    {

        static public List<string> GetSunks(GamePlayer gamePlayer)
        {

            GamePlayer gamePlayerOp = gamePlayer.GetOpponent();

            List<string> listabarco = new List<string>();


            //recorre los barcos del gamePlayer que ve el juego
            foreach (Ship barco in gamePlayer.Ships)
            {
                var conter = 0;
                //recorre las ubicaciones de los barcos del gamePlayer que ve el juego
                foreach (ShipLocation locbarco in barco.Locations)
                {
                    //recorre los salvos que hizo el oponente
                    foreach (Salvo tiros in gamePlayerOp.Salvos)
                    {

                        //recorre las ubicaciones de los salvos que hizo el oponente
                        foreach (SalvoLocations locsalvo in tiros.Locations)
                        {
                            //si las ubicaciones del barco y de los tiros del oponente coinciden
                            //va al contador
                            if (locsalvo.Location == locbarco.Location)
                            {
                                conter++;
                            }
                        }

                    }
                }
                //luego de recorrer y hacer la comparación de un solo barco 
                //agrega a la lista de barcos el nombre del barco
                if (conter >= barco.Locations.Count())
                {
                    listabarco.Add(barco.Type);
                }
            }

            return listabarco;
        }
    }
}
