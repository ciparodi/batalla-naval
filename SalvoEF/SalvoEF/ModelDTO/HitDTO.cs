﻿using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class HitDTO
    {
        public long Turn { get; set; }
        public List<HitLocationDTO> Hits { get; set; }



        public HitDTO(Salvo salvo, List<Ship> ships)
        {
            Turn = salvo.Turn;
            Hits = HitLocationDTO.ListHitLocationDTO(ships, salvo);
        }

        static public List<HitDTO> ListHitDTO (List<Salvo> salvos, List<Ship>ships)
        {
            return salvos.Select(x => new HitDTO(x, ships)).ToList();
        }

    }
}
