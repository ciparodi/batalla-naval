﻿using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace salvoEF
{
    public class PlayerDTO
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public PlayerDTO(){ }

        public PlayerDTO(Player player)
        {
            Id = player.Id;
            Email = player.Email;          
        }
    }
}
