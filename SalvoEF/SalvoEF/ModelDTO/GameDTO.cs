﻿using Microsoft.AspNetCore.Http;
using SalvoEF.ModelDTO;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace salvoEF
{
    public class GameDTO
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public List<GamePlayerViewDTO> GamePlayers { get; set; }

        public GameDTO(Game game)
        {
            CreationDate = game.CreationDate;
            Id = game.Id;
            GamePlayers = game.GamePlayer.Select(x => new GamePlayerViewDTO(x)).ToList();
        }
       
    }

    public class GameAuthDTO
    {
        public string Email { get; set; }
        public List<GameDTO> Games { get; set; }
        
        public GameAuthDTO()
        {

        }

    }
}
        
