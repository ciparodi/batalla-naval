﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class SalvoDTO
    {
        public long Id { get; set; }
        public long Turn { get; set; }        
        public List<SalvoLocationDTO> Locations { get; set; }
        public PlayerDTO Player { get; set; }

        public SalvoDTO() { }

        public SalvoDTO (Salvo salvo)
        {
            Turn = salvo.Turn;
            Id = salvo.Id;
            Player = new PlayerDTO(salvo.GamePlayer.Player);
            Locations = salvo.Locations.Select(x => new SalvoLocationDTO(x)).ToList();

        }

        static public List<SalvoDTO> ListSalvoDTO(GamePlayer gamePlayer)
        {

            List<SalvoDTO> listSalvo = new List<SalvoDTO>();

            foreach (var GP in gamePlayer.Game.GamePlayer)
            {
                var gDTO = new GamePlayerViewDTO(GP);
                foreach (var sal in GP.Salvos)
                {
                    var salGTO = new SalvoDTO(sal);
                    listSalvo.Add(salGTO);

                }
            }

            return listSalvo;

        }


        static public Salvo SalvoDTOtoSalvo(SalvoDTO saldto)
        {
            Salvo newsalvo = new Salvo();
            newsalvo.Turn = saldto.Turn;
            newsalvo.Locations = new List<SalvoLocations>();

            foreach (SalvoLocationDTO loc in saldto.Locations)
            {
                SalvoLocations locacion = new SalvoLocations();
                locacion.Location = loc.Location;
                newsalvo.Locations.Add(locacion);
            }

            return newsalvo;
        }
    }
}
