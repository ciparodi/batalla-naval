﻿using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class ShipDTO
    {
        public string Type { get; set; }
        public List<ShipLocationDTO> Locations { get; set; }


        public ShipDTO() { }


        public ShipDTO(Ship ship)
        {
            Type = ship.Type;
            Locations = ship.Locations.Select(x => new ShipLocationDTO(x)).ToList();
        }

        static public List<ShipDTO> ListShipDTO(List<Ship> ships)
        {
            return ships.Select(x => new ShipDTO(x)).ToList();
        }

        static public Ship ShipDTOtoShip(ShipDTO sdto)
        {
            Ship newship = new Ship();
            newship.Type = sdto.Type;
            newship.Locations = new List<ShipLocation>();
            //sdto.Locations.Select(x => new ShipLocation(x)).ToList();

            foreach (ShipLocationDTO loc in sdto.Locations)
            {
                ShipLocation locacion = new ShipLocation();
                locacion.Location = loc.Location;
                newship.Locations.Add(locacion);

            }

            return newship;

        }
    }
}
