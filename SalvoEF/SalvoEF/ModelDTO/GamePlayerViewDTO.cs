﻿using SalvoEF.ModelDTO;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace salvoEF
{
    public class GamePlayerViewDTO
    {
        public long Id { get; set; }
        public DateTime JoinDate { get; set; }
        public double? Point { get; set; }
        public PlayerDTO Player { get; set; }



        public GamePlayerViewDTO(GamePlayer gameplayer)
        {
            Id = gameplayer.Id;
            Player = new PlayerDTO(gameplayer.Player);
            Point = gameplayer.GetScore() != null ? gameplayer.GetScore().Point : null;
        }

        
    }
}



