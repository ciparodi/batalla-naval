﻿using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class ShipLocationDTO
    {
        public long Id { get; set; }
        public string Location { get; set; }

        public ShipLocationDTO()
        {
        }
        public ShipLocationDTO(ShipLocation shipLocation)
        {
            Id = shipLocation.Id;
            Location = shipLocation.Location;
        }

    }
}
