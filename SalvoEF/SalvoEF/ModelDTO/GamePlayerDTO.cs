﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class GamePlayerDTO
    {
        public long Id { get; set; }
        public DateTime JoinDate { get; set; }
        public PlayerDTO Player { get; set; }


        public GamePlayerDTO(GamePlayer gameplayer)
        {
            Id = gameplayer.Id;
            Player = new PlayerDTO(gameplayer.Player);
        }          
        
    }
}
