﻿using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class SalvoLocationDTO
    {
        public long Id { get; set; }
        public string Location { get; set; }

        public SalvoLocationDTO() { }

        public SalvoLocationDTO(SalvoLocations salvoLocation)
        {
            Id = salvoLocation.Id;
            Location = salvoLocation.Location;
        }

    }
}
