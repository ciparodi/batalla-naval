﻿using salvoEF;
using SalvoEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.ModelDTO
{
    public class GameViewDTO
    {
        public long Id { get; set; }
        public DateTime? CreationDate { get; set; }
        public List<GamePlayerViewDTO> GamePlayers { get; set; }
        public List<ShipDTO> Ships { get; set; }
        public List<SalvoDTO> Salvos { get; set; }
        public List<HitDTO> Hits { get; set; }
        public List<string> Sunks { get; set; }
        public List<HitDTO> HitsOpponent { get; set; }
        public List<string> SunksOpponent { get; set; }
        public string GameState { get; set; }

        public GameViewDTO(GamePlayer gamePlayerdb)
        {
            CreationDate = gamePlayerdb.Game.CreationDate;
            Id = gamePlayerdb.Id;

            GamePlayers = gamePlayerdb.Game.GamePlayer.Select(x => new GamePlayerViewDTO(x)).ToList();

            Ships = ShipDTO.ListShipDTO(gamePlayerdb.Ships);
            Salvos = SalvoDTO.ListSalvoDTO(gamePlayerdb);



            if (gamePlayerdb.GetOpponent() != null)
            {
                SunksOpponent = SunksDTO.GetSunks(gamePlayerdb.GetOpponent());
                Sunks = SunksDTO.GetSunks(gamePlayerdb);

                Hits = HitDTO.ListHitDTO(gamePlayerdb.Salvos, gamePlayerdb.GetOpponent().Ships);
                HitsOpponent = HitDTO.ListHitDTO(gamePlayerdb.GetOpponent().Salvos, gamePlayerdb.Ships);
            }

            else
            {
                SunksOpponent = new List<string>();
                Sunks = new List<string>();

                Hits = new List<HitDTO>();
                HitsOpponent = new List<HitDTO>();
            }

            GameState = gamePlayerdb.GetState();
        }
    }
}


