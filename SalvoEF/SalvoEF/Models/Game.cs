﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Models
{
    public class Game
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public List<GamePlayer> GamePlayer { get; set; }
        public List<Score> Scores { get; set; }
    }
}
