﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Models
{
    public class Ship
    {
        public long Id { get; set; }
        public long Length { get; set; }
        public string Type { get; set; }
        public GamePlayer Gameplayer { get; set; }
        public long GamePlayerID { get; set; }
        public ICollection<ShipLocation> Locations { get; set; }
    }
}
