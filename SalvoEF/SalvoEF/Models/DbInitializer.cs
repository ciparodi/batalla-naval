﻿using SalvoEF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Models
{
    static public class DbInitializer
    {
        public static void Initialize(MinHubContext context)
        {
            if (!context.Player.Any())
            {
                var players = new Player[]
                {
                    new Player {Email = "matias@gmail.com", Password = "sakuracardcaptor"},
                    new Player {Email = "juanberto@gmail.com", Password = "monkeymagic"},
                    new Player {Email = "rigoberta@gmail.com", Password = "ruinas1234"},
                    new Player {Email = "juanpedrodelvalle@gmail.com", Password = "rubenteamo"}

                };

                foreach (Player p in players)
                {
                    context.Player.Add(p);
                }

                context.SaveChanges();
            }

            if (!context.Game.Any())
            {
                var games = new Game[]
                {
                    new Game {CreationDate = DateTime.Now},
                    new Game {CreationDate = DateTime.Now.AddHours(1)},
                    new Game {CreationDate = DateTime.Now.AddHours(2)},
                    new Game {CreationDate = DateTime.Now.AddHours(3)},
                    new Game {CreationDate = DateTime.Now.AddHours(4)},
                    new Game {CreationDate = DateTime.Now.AddHours(5)},
                    new Game {CreationDate = DateTime.Now.AddHours(6)},
                    new Game {CreationDate = DateTime.Now.AddHours(7)}
                };

                foreach (Game g in games)
                {
                    context.Game.Add(g);
                }

                context.SaveChanges();
            }

            if (!context.GamePlayer.Any())
            {
                Game game1 = context.Game.Find(1L);
                Game game2 = context.Game.Find(2L);
                Game game3 = context.Game.Find(3L);
                Game game4 = context.Game.Find(4L);
                Game game5 = context.Game.Find(5L);
                Game game6 = context.Game.Find(6L);
                Game game7 = context.Game.Find(7L);
                Game game8 = context.Game.Find(8L);

                Player matias = context.Player.Find(1L);
                Player juanberto = context.Player.Find(2L);
                Player rigoberta = context.Player.Find(3L);
                Player juanpedrodelvalle = context.Player.Find(4L);


                var gamesPlayers = new GamePlayer[]
                {
                    new GamePlayer{JoinDate=DateTime.Now, Game = game1, Player = matias},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game1, Player = juanberto},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game2, Player = matias},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game2, Player = juanberto},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game3, Player = juanberto},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game3, Player = juanpedrodelvalle},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game4, Player = juanberto},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game4, Player = matias},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game5, Player = juanpedrodelvalle},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game5, Player = matias},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game6, Player = rigoberta},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game7, Player = juanpedrodelvalle},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game8, Player = rigoberta},
                    new GamePlayer{JoinDate=DateTime.Now, Game = game8, Player = juanpedrodelvalle},
                };
                foreach (GamePlayer gp in gamesPlayers)
                {
                    context.GamePlayer.Add(gp);
                }
                context.SaveChanges();
            }
            
            if (!context.Ship.Any())
            {

                GamePlayer gamePlayer1 = context.GamePlayer.Find(1L);
                GamePlayer gamePlayer2 = context.GamePlayer.Find(2L);
                GamePlayer gamePlayer3 = context.GamePlayer.Find(3L);
                GamePlayer gamePlayer4 = context.GamePlayer.Find(4L);
                GamePlayer gamePlayer5 = context.GamePlayer.Find(5L);
                GamePlayer gamePlayer6 = context.GamePlayer.Find(6L);
                GamePlayer gamePlayer7 = context.GamePlayer.Find(7L);
                GamePlayer gamePlayer8 = context.GamePlayer.Find(8L);
                GamePlayer gamePlayer9 = context.GamePlayer.Find(9L);
                GamePlayer gamePlayer10 = context.GamePlayer.Find(10L);
                GamePlayer gamePlayer11 = context.GamePlayer.Find(11L);
                GamePlayer gamePlayer12 = context.GamePlayer.Find(12L);
                GamePlayer gamePlayer13 = context.GamePlayer.Find(13L);
                GamePlayer gamePlayer14 = context.GamePlayer.Find(14L);

                var ships = new Ship[]
                {
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer1, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "H2" },
                            new ShipLocation { Location = "H2" },
                            new ShipLocation { Location = "H3" }
                        }
                    },
                    new Ship{Type = "Submarine", Gameplayer = gamePlayer1, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "E1" },
                            new ShipLocation { Location = "F1" },
                            new ShipLocation { Location = "G1" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer1, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B4" },
                            new ShipLocation { Location = "B5" }
                        }
                    },
                    // gp2
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer2
                    , Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer2
                    , Locations = new ShipLocation[] {
                            new ShipLocation { Location = "F1" },
                            new ShipLocation { Location = "F2" }
                        }
                    },
                    // gp3
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer3, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer3, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "C6" },
                            new ShipLocation { Location = "C7" }
                        }
                    },
                    // gp4
                    new Ship{Type = "Submarine", Gameplayer = gamePlayer4, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "A2" },
                            new ShipLocation { Location = "A3" },
                            new ShipLocation { Location = "A4" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer4, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "G6" },
                            new ShipLocation { Location = "H6" }
                        }
                    },
                    // gp5
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer5, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer5, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "C6" },
                            new ShipLocation { Location = "C7" }
                        }
                    },
                    // gp6
                    new Ship{Type = "Submarine", Gameplayer = gamePlayer6, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "A2" },
                            new ShipLocation { Location = "A3" },
                            new ShipLocation { Location = "A4" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer6, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "G6" },
                            new ShipLocation { Location = "H6" }
                        }
                    },
                    // gp7
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer7, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer7, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "C6" },
                            new ShipLocation { Location = "C7" }
                        }
                    },
                    // gp8
                    new Ship{Type = "Submarine", Gameplayer = gamePlayer8, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "A2" },
                            new ShipLocation { Location = "A3" },
                            new ShipLocation { Location = "A4" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer8, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "G6" },
                            new ShipLocation { Location = "H6" }
                        }
                    },
                    // gp9
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer9, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer9, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "C6" },
                            new ShipLocation { Location = "C7" }
                        }
                    },
                    //matias gp10
                    new Ship{Type = "Submarine", Gameplayer = gamePlayer10, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "A2" },
                            new ShipLocation { Location = "A3" },
                            new ShipLocation { Location = "A4" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer10, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "G6" },
                            new ShipLocation { Location = "H6" }
                        }
                    },
                    // gp11
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer11, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer11, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "C6" },
                            new ShipLocation { Location = "C7" }
                        }
                    },
                    // gp12
                    new Ship{Type = "Destroyer", Gameplayer = gamePlayer12, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "B5" },
                            new ShipLocation { Location = "C5" },
                            new ShipLocation { Location = "D5" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer12, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "C6" },
                            new ShipLocation { Location = "C7" }
                        }
                    },
                    // gp13
                    new Ship{Type = "Submarine", Gameplayer = gamePlayer13, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "A2" },
                            new ShipLocation { Location = "A3" },
                            new ShipLocation { Location = "A4" }
                        }
                    },
                    new Ship{Type = "PatroalBoat", Gameplayer = gamePlayer13, Locations = new ShipLocation[] {
                            new ShipLocation { Location = "G6" },
                            new ShipLocation { Location = "H6" }
                        }
                    },
                };
                

                foreach (Ship s in ships)
                {
                    context.Ship.Add(s);
                }

                context.SaveChanges();
            }
        
            if (!context.GamePlayer.Any())
            {
                var gamePlayers = new GamePlayer[]
                {
                    new GamePlayer {JoinDate = DateTime.Now},
                    new GamePlayer {JoinDate = DateTime.Now.AddHours(1)},
                    new GamePlayer {JoinDate = DateTime.Now.AddHours(2)},
                    new GamePlayer {JoinDate = DateTime.Now.AddHours(3)}

                };

                foreach (GamePlayer gp in gamePlayers)
                {
                    context.GamePlayer.Add(gp);
                }

                context.SaveChanges();
            }

            if (!context.Salvo.Any())
            {
                GamePlayer gamePlayer1 = context.GamePlayer.Find(1L);
                GamePlayer gamePlayer2 = context.GamePlayer.Find(2L);
                GamePlayer gamePlayer3 = context.GamePlayer.Find(3L);
                GamePlayer gamePlayer4 = context.GamePlayer.Find(4L);
                GamePlayer gamePlayer5 = context.GamePlayer.Find(5L);
                GamePlayer gamePlayer6 = context.GamePlayer.Find(6L);
                GamePlayer gamePlayer7 = context.GamePlayer.Find(7L);
                GamePlayer gamePlayer8 = context.GamePlayer.Find(8L);
                GamePlayer gamePlayer9 = context.GamePlayer.Find(9L);
                GamePlayer gamePlayer10 = context.GamePlayer.Find(10L);
                GamePlayer gamePlayer11 = context.GamePlayer.Find(11L);
                GamePlayer gamePlayer12 = context.GamePlayer.Find(12L);
                GamePlayer gamePlayer13 = context.GamePlayer.Find(13L);



                var salvoes = new Salvo[]
                {
                    new Salvo {Turn = 1, GamePlayer = gamePlayer1, Locations = new SalvoLocations [] {
                            new SalvoLocations { Location = "B5" },
                            new SalvoLocations { Location = "C5" },
                            new SalvoLocations { Location = "F1" },} },

                    new Salvo {Turn = 2, GamePlayer = gamePlayer2, Locations = new SalvoLocations[]
                            {new SalvoLocations { Location = "F2" },
                            new SalvoLocations { Location = "D5" }}},
            
                    new Salvo {Turn = 1, GamePlayer = gamePlayer3, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "B4" },
                            new SalvoLocations { Location = "B5" },
                            new SalvoLocations { Location = "B6" }
                    }
                    },
                    new Salvo {Turn = 2, GamePlayer = gamePlayer4, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "E1" },
                            new SalvoLocations { Location = "H3" },
                            new SalvoLocations { Location = "A2" }
                    }
                    },
                    new Salvo {Turn = 1, GamePlayer = gamePlayer5, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "A2" },
                            new SalvoLocations { Location = "A4" },
                            new SalvoLocations { Location = "G6" }
                    }
                    },
                    new Salvo {Turn = 2, GamePlayer = gamePlayer6, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "H1" },
                            new SalvoLocations { Location = "H2" },
                            new SalvoLocations { Location = "H3" }
                    }
                    },
                    new Salvo {Turn = 1, GamePlayer = gamePlayer7, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "E1" },
                            new SalvoLocations { Location = "F2" },
                            new SalvoLocations { Location = "G3" }
                    }
                    },
                    new Salvo {Turn = 2, GamePlayer = gamePlayer8, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "A3" },
                            new SalvoLocations {Location = "A4" },
                            new SalvoLocations { Location = "F7" }
                    }
                    },
                    new Salvo {Turn = 1, GamePlayer = gamePlayer9, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "A2" },
                            new SalvoLocations { Location = "G6" },
                            new SalvoLocations { Location = "H6" }
                    }
                    },
                    new Salvo {Turn = 2, GamePlayer = gamePlayer10, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "B5" },
                            new SalvoLocations { Location = "C6" },
                            new SalvoLocations { Location = "H1" }
                    }
                    },
                    new Salvo {Turn = 3, GamePlayer = gamePlayer11, Locations = new SalvoLocations[]
                    {
                            new SalvoLocations { Location = "C5" },
                            new SalvoLocations { Location = "C7" },
                            new SalvoLocations { Location = "D5" }
                    }
                    }
                };

                foreach (Salvo s in salvoes)
                {
                    context.Salvo.Add(s);
                }

                context.SaveChanges();
            }
            
            if (!context.Scores.Any())
            {
                Game game1 = context.Game.Find(1L);
                Game game2 = context.Game.Find(2L);
                Game game3 = context.Game.Find(3L);
                Game game4 = context.Game.Find(4L);
                Game game5 = context.Game.Find(5L);
                Game game6 = context.Game.Find(6L);
                Game game7 = context.Game.Find(7L);
                Game game8 = context.Game.Find(8L);

                Player matias = context.Player.Find(1L);
                Player juanberto = context.Player.Find(2L);
                Player rigoberta = context.Player.Find(3L);
                Player juanpedrodelvalle = context.Player.Find(4L);

                var scores = new Score[]
                {
                    //matias gp1
                    new Score {
                        Game = game1,
                        Player = matias,
                        FinishDate = DateTime.Now,
                        Point = 1
                    },

                    //juanbertogp2
                    new Score {
                        Game = game1,
                        Player = juanberto,
                        FinishDate = DateTime.Now,
                        Point = 0
                    },

                    //matias gp3
                    new Score {
                        Game = game2,
                        Player = matias,
                        FinishDate = DateTime.Now,
                        Point = 0.5
                    },

                    //juanbertogp4
                    new Score {
                        Game = game2,
                        Player = juanberto,
                        FinishDate = DateTime.Now,
                        Point = 0.5
                    },

                    //juanbertogp5
                    new Score {
                        Game = game3,
                        Player = juanberto,
                        FinishDate = DateTime.Now,
                        Point = 0
                    },

                    //almeida gp6
                    new Score {
                        Game = game3,
                        Player = juanpedrodelvalle,
                        FinishDate = DateTime.Now,
                        Point = 1
                    },

                    //juanbertogp7
                    new Score {
                        Game = game4,
                        Player = juanberto,
                        FinishDate = DateTime.Now,
                        Point = 0.5
                    },

                    //matias gp8
                    new Score {
                        Game = game4,
                        Player = matias,
                        FinishDate = DateTime.Now,
                        Point = 0.5
                    },
                };

                foreach (Score score in scores)
                {
                    context.Scores.Add(score);
                }

                context.SaveChanges();
            }
        }
    }
}
