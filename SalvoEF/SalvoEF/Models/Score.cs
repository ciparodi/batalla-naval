﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Models
{
    public class Score
    {
        public long Id { get; set; }
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public double? Point { get; set; }
        public DateTime FinishDate { get; set; }
        public Player Player { get; set; }
        public Game Game { get; set; }


        static public Score GetScore(GamePlayer gamePlayer)
        {
            Score puntajeJugador = new Score();
            puntajeJugador.GameId = gamePlayer.Gameid;
            puntajeJugador.PlayerId = gamePlayer.Playerid;
            puntajeJugador.FinishDate = DateTime.Now;

            if (gamePlayer.GetState() == "WIN")
            {
                puntajeJugador.Point = 1.0;

            }

            if (gamePlayer.GetState() == "LOSS")
            {
                puntajeJugador.Point = 0;
            }

            if (gamePlayer.GetState() == "TIE")
            {
                puntajeJugador.Point = 0.5;
            }

            return puntajeJugador;
        }

    }
}
