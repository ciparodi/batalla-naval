﻿using SalvoEF.ModelDTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SalvoEF.Models
{
    public class GamePlayer
    {
        public long Id { get; set; }
        public DateTime? JoinDate { get; set; }
        public Game Game { get; set; }
        public Player Player { get; set; }
        [ForeignKey("player")]
        public long Playerid { get; internal set; }
        [ForeignKey("Playerid")]
        public long Gameid { get; set; }
        public List<Ship> Ships { get; set; }
        public List<Salvo> Salvos { get; set; }

        public Score GetScore()
        {
            return Player.GetScore(Game);
        }
        
        public GamePlayer GetOpponent()
        {
            return Game.GamePlayer.Where(gp => gp.Id != Id)
                                    .FirstOrDefault();
        }

        public string GetState()
        {
            string Status = "WAIT";

            if (this.GetOpponent() != null)
            {

                //valida si hay más de un jugador en juego
                if (this.Game.GamePlayer.Count() >= 1)
                {
                    Status = "PLACE_SHIPS";

                    //valida si mis barcos están colocados
                    if (this.Ships.Count() != 0)
                    {
                        Status = "WAIT";

                        //valida si también los barcos del op están colocados
                        //Y si es mi turno
                        if (this.GetOpponent().Ships.Count() != 0)
                        {
                            if (MyTurn())
                            {
                                Status = "ENTER_SALVO";

                                if (GameOver())
                                {
                                    if (SunksDTO.GetSunks(this).Count() == this.Ships.Count())
                                    {
                                        Status = "LOSS";
                                    }

                                    if (SunksDTO.GetSunks(this.GetOpponent()).Count() == this.GetOpponent().Ships.Count())
                                    {
                                        Status = "WIN";
                                    }

                                    if (SunksDTO.GetSunks(this).Count() == SunksDTO.GetSunks(this.GetOpponent()).Count())
                                    {
                                        Status = "TIE";
                                    }
                                }

                            }

                            else
                            {
                                Status = "WAIT";
                                if (GameOver())
                                {
                                    if (SunksDTO.GetSunks(this).Count() == this.Ships.Count())
                                    {
                                        Status = "LOSS";
                                    }

                                    if (SunksDTO.GetSunks(this.GetOpponent()).Count() == this.GetOpponent().Ships.Count())
                                    {
                                        Status = "WIN";
                                    }

                                    if (SunksDTO.GetSunks(this).Count() == SunksDTO.GetSunks(this.GetOpponent()).Count())
                                    {
                                        Status = "TIE";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return Status;
        }

        
        private bool MyTurn()
        {
                        //si disparamos la misma cantidad de veces y si soy creador
            return (    (this.Salvos.Count() == this.GetOpponent().Salvos.Count()
                            && this.JoinDate < this.GetOpponent().JoinDate)
                    ||  
                        //si dispare menos veces y no soy creador
                        (this.Salvos.Count() < this.GetOpponent().Salvos.Count()
                            && this.JoinDate > this.GetOpponent().JoinDate));
        }

        public bool GameOver()
        {               
                        //si le hundí todos los barcos o me hundió todos los barcos
            return (    (SunksDTO.GetSunks(this).Count() == this.Ships.Count()                
                            || SunksDTO.GetSunks(this.GetOpponent()).Count() == this.GetOpponent().Ships.Count())

                    &&
                        //si ambos disparamos la misma cantidad de veces
                        (this.Salvos.Count() == this.GetOpponent().Salvos.Count()));
        }


    }
}
